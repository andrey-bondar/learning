<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Course;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(ManagerRegistry $registry, SessionInterface $session)
    {
        $this->session = $session;
        parent::__construct($registry, User::class);
    }

    public function findOneByEmailAndCourse(string $email, Course $course): ?User
    {
        $query = $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->andWhere('u.course = :course')
            ->setParameters([
                'email' => $email,
                'course' => $course,
            ])
            ->getQuery()
        ;

        return $query->getOneOrNullResult();
    }

    public function loadUserByUsername($username)
    {
        $course = $this->session->get('course');

        $query = $this->createQueryBuilder('u')
            ->where('u.username = :username')
            ->andWhere('u.course = :course')
            ->setParameters([
                'username' => $username,
                'course' => $course,
            ])
            ->getQuery()
        ;

        return $query->getOneOrNullResult();
    }
}
