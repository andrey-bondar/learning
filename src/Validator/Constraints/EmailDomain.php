<?php

declare(strict_types=1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EmailDomain extends Constraint
{
    public $domains;

    public $message = 'The email "%email%" has not a valid domain.';

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->domains = $options['domains'] ?? [];
        $this->message = $options['message'] ?? $this->message;

        parent::__construct($options);
    }
}
