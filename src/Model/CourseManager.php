<?php

declare(strict_types=1);

namespace App\Model;

use App\Repository\CourseRepository;

class CourseManager
{
    /**
     * @var CourseRepository
     */
    private $repository;

    /**
     * @param CourseRepository $repository
     */
    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return CourseRepository
     */
    public function getRepository(): CourseRepository
    {
        return $this->repository;
    }
}
