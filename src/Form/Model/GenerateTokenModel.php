<?php

declare(strict_types=1);

namespace App\Form\Model;

class GenerateTokenModel
{
    /**
     * @var string
     */
    private $email;

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return GenerateTokenModel
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
