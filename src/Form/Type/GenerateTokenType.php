<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Form\Model\GenerateTokenModel;
use App\Validator\Constraints\EmailDomain;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GenerateTokenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'constraints' => [
                    new EmailDomain(['domains' => ['chumak.com'], 'message' => 'Для доступу необхідна електронна адреса з домену chumak.com']),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GenerateTokenModel::class,
        ]);
    }
}
