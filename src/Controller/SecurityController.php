<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\CourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function login(AuthenticationUtils $authenticationUtils, SessionInterface $session, Request $request, CourseRepository $courseRepository)
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('course_list');
        }

        if (null !== $request->get('course')) {
            $session->set('course', $request->get('course'));
        }

        if (null === $session->get('course')) {
            throw $this->createNotFoundException();
        }

        $course = $courseRepository->find($session->get('course'));

        if (null === $course) {
            return $this->redirectToRoute('course_list');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
            'course'        => $course,
        ));
    }
}
