<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Course;
use App\Entity\User;
use App\Form\Model\GenerateTokenModel;
use App\Form\Type\GenerateTokenType;
use App\Model\CourseManager;
use App\Model\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CourseController extends Controller
{
    const ERROR_PASSWORD_EXISTS = 1;

    public function listAction(CourseManager $courseManager): Response
    {
        $courses = $courseManager->getRepository()->findAll();

        return $this->render('course/list/list.html.twig', [
            'courses' => $courses,
        ]);
    }

    public function request(Request $request, Course $course, FormFactoryInterface $formFactory, UserManager $userManager, \Swift_Mailer $mailer, EntityManagerInterface $em): Response
    {
        $model = new GenerateTokenModel();

        $form = $formFactory->create(GenerateTokenType::class, $model);
        $form->handleRequest($request);

        $errors = [];

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $userManager->getRepository()->findOneByEmailAndCourse($model->getEmail(), $course);

            if (null === $user) {
                $user = new User();
                $user
                    ->setEmail($model->getEmail())
                    ->setUsername($model->getEmail())
                    ->setCourse($course)
                ;
                $em->persist($user);
            }

            if ($user->isExpired()) {
                try {
                    $em->beginTransaction();

                    $user
                        ->setPassword(substr(hash('sha512', (string) rand()), 0, 64))
                        ->setPasswordDate(new \DateTime('now'))
                    ;

                    $em->flush();

                    $message = (new \Swift_Message($this->getParameter('mails')['request_course']['subject']))
                        ->setFrom($this->getParameter('mail_from'))
                        ->setTo($user->getEmail())
                        ->setBody(
                            $this->renderView(
                                'emails/generated_password.html.twig',
                                [
                                    'user' => $user,
                                    'course' => $course,
                                ]
                            ),
                            'text/html'
                        )
                    ;

                    $mailer->send($message);

                    $em->commit();

                    return $this->render('course/request/password_sent.html.twig', [
                        'user' => $user,
                    ]);
                } catch (\Exception $e) {
                    $em->rollback();

                    throw $e;
                }
            } else {
                $errors[] = self::ERROR_PASSWORD_EXISTS;
            }
        }

        return $this->render('course/request/request.html.twig', [
            'form' => $form->createView(),
            'course' => $course,
            'errors' => $errors,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     */
    public function view(Course $course, TokenStorageInterface $tokenStorage): Response
    {
        $user = $tokenStorage->getToken()->getUser();

        if (!$user instanceof User) {
            throw $this->createNotFoundException();
        }

        if ($user->getCourse() !== $course) {
            return $this->render('course/view/unavailable.html.twig');
        }

        return $this->render('course/view/view.html.twig', [
            'course' => $course,
        ]);
    }
}
