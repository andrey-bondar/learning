<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Controller\CourseController;
use App\Entity\Course;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerArgumentsEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class SecuritySubscriber implements EventSubscriberInterface
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER_ARGUMENTS => ['onController'],
        ];
    }

    public function onController(FilterControllerArgumentsEvent $event): void
    {
        list($controller, $method) = $event->getController();

        if (!$controller instanceof CourseController || 'view' !== $method) {
            return;
        }

        $course = null;

        foreach ($event->getArguments() as $argument) {
            if (!$argument instanceof Course) {
                continue;
            }

            $course = $argument;
        }


        if (null === $course) {
            throw new \Exception('Course should be denied for action');
        }

        $this->session->set('course', $course->getId());
    }
}
