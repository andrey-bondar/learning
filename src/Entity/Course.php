<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CourseRepository")
 * @ORM\Table(name="cource")
 */
class Course
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", nullable=false)
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(name="directory", type="string", nullable=false)
     *
     * @var string
     */
    private $directory;

    /**
     * @ORM\Column(name="active", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $active = true;

    /**
     * @ORM\Column(name="url", type="string", nullable=false)
     *
     * @var string
     */
    private $url;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Course|null
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return Course
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return string
     */
    public function getDirectory(): ?string
    {
        return $this->directory;
    }

    /**
     * @param string $directory
     *
     * @return Course
     */
    public function setDirectory(string $directory): self
    {
        $this->directory = $directory;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return Course
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function __toString()
    {
        return sprintf('#%d %s', $this->getId(), $this->getTitle());
    }
}
