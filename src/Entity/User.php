<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="user_unique", columns={"username", "course_id"})
 * })
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="email", type="string", length=128, nullable=false)
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(name="password_date", type="datetime", nullable=false)
     *
     * @var \DateTime
     */
    private $passwordDate;

    /**
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", nullable=false)
     *
     * @var Course
     */
    private $course;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function eraseCredentials(): void
    {
    }

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ]);
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
        ) = unserialize($serialized, ['allowed_classes' => false]);
    }

    /**
     * @return \DateTime
     */
    public function getPasswordDate(): ?\DateTime
    {
        return $this->passwordDate;
    }

    /**
     * @param \DateTime $passwordDate
     *
     * @return User
     */
    public function setPasswordDate(\DateTime $passwordDate): self
    {
        $this->passwordDate = $passwordDate;

        return $this;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        return null === $this->getPasswordDate()
            || $this->getPasswordDate() < new \DateTime('-1 Day');
    }

    /**
     * @return Course
     */
    public function getCourse(): Course
    {
        return $this->course;
    }

    /**
     * @param Course $course
     *
     * @return User
     */
    public function setCourse(Course $course): User
    {
        $this->course = $course;

        return $this;
    }
}
