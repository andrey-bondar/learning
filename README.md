## Installation

1. Copy file .env.dist to .env.
2. Configure parameters in .env 
3. Run in console:
```bash
composer install
bin/console doctrine:schema:update --force
```

## Web server configuration
Nginx / Apache config:
https://symfony.com/doc/current/setup/web_server_configuration.html

## Administration Area
Admin:
admin / admin

```sql
DROP INDEX UNIQ_8D93D649E7927C74 ON user;
DROP INDEX UNIQ_8D93D649F85E0677 ON user;
CREATE UNIQUE INDEX user_unique ON user (username, course_id);
```
